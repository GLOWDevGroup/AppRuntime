[![Generic badge](https://img.shields.io/badge/version-unreleased-red.svg)](https://shields.io/) [![Generic badge](https://img.shields.io/badge/GLOWOS-signed-blue.svg)](https://shields.io/)

# App Runtime Library

### Information about this Library

This Library runs applications in the ```.APP``` format. It can asks the system to check for Updates or for ```Administrative Privilege```, if required.
Depending on the Settings of the ```<GLOWOS Core Security Environment>```, it will check the Signature of the Application before running it. As a security measurement, the applications will run in sandboxed mode and won't have direct access to the System. By default they can only use APIs installed and enabled on the System. For thing such as Camera or File System access , the System will ask the user for Permissions to these APIs.
